# coding: utf-8
require 'twitter'

Twitter.configure do |config|
  config.consumer_key    = 'YOUR_CONSUMER_KEY'
  config.consumer_secret = 'YOUR_CONSUMER_SECRET'
end

@client = Twitter::Client.new(
  oauth_token:        'YOUR_ACCESS_TOKEN',
  oauth_token_secret: 'YOUR_ACCESS_TOKEN_SECRET'
)

@client.search(ARGV[0] || '#ruby').results.each do |tweet|
  puts "@#{tweet.from_user} #{tweet.created_at} -- #{tweet.text}"
end