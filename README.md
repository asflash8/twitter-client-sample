TwitterクライアントをElasticBeanstalkで動かそう

Twitterアカウントの取得
-----
1. https://twitter.com でアカウントを取得
1. https://dev.twitter.com でアプリケーションを作成
  - [Manage & create your applications](https://dev.twitter.com/apps) で必要項目を入力  
  - settingsタブのApplication typeを「Read and Write」に変更  
  - detailsタブの一番下にある「Create my access token」ボタンでアクセストークンを生成  
  - detailsタブにある次の項目をメモ（プログラムで使います）  
　Consumer key  
　Consumer secret  
　Access token  
　Access token secret

事前準備（導入済みの場合はスキップ）
-----
1  Rubyの実行環境のインストール

  るびまの[Rubyの歩き方](http://magazine.rubyist.net/?FirstStepRuby)あたりを参考にruby(1.9.3)インストール（おすすめはrbenv + ruby-build)

2  Rubyのライブラリ導入管理ライブラリのbundlerをインストール

    gem install bundler

Twitterライブラリのインストール
-----
1  適当なディレクトリに移動

    mkdir -p ~/devel/twitter-client && cd ~/devel/twitter-client

2  bundlerの初期化

    bundle init

3  twitterライブラリの設定をGemfileに追加

    echo "gem 'twitter'" >> Gemfile

4  twitterライブラリのインストール

    bundle install --path vendor/bundle